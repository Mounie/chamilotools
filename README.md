# Chamilo tools

Chamilo tools is a set of tools to interact with a
[Chamilo](https://chamilo.org/) server without using your browser.

Right now, it contains:

* setChamilo.sh: a shell script to upload the content of your course
  in one command.

* createQuizChamilo.py: a Python script to write quiz (Exercices)
  offline in a XLS file or a [Yaml](http://yaml.org/) file, and upload
  it to Chamilo in one command.

It has been written by and for Ensimag teachers, but could easily be
adapted to other Chamilo installation. Patches to make the tools more
generic and let them work in more contexts are welcome.
