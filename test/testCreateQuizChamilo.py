#!/usr/bin/env python2

import sys
import os
TEST_DIR = os.path.dirname(__file__)
SCRIPT_DIR = os.path.join(os.path.dirname(__file__), "..")
SCRIPT = os.path.join(SCRIPT_DIR, "createQuizChamilo.py")
sys.path.append(SCRIPT_DIR)

from createQuizChamilo import (
    openQuiz,
    yaml2quiz,
    quiz2yaml,
    vectorize,
    flatten
)
from testCreateDownloadQuiz import TEST_FILES


import unittest
import subprocess


try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    import os
    DEVNULL = open(os.devnull, 'wb')


class TestVectorize(unittest.TestCase):
    def setUp(self):
        self.dict = {'a': 'b', 'c': 'd'}
        self.vect = [{'a': 'b'}, {'c': 'd'}]

    def testVectorize(self):
        v = vectorize(self.dict)
        self.assertEquals(v, self.vect)

    def testFlatten(self):
        d = flatten(self.vect)
        self.assertEquals(d, self.dict)


class TestQuiz2Yaml2Quiz(unittest.TestCase):
    def runTest(self):
        for basename in TEST_FILES:
            file = os.path.join(os.path.dirname(__file__), basename)
            print "Testing %s ..." % file
            d, a = openQuiz(file)
            y = quiz2yaml(d, a)
            D, A = yaml2quiz(y)
            self.assertEquals(D, d)
            self.assertEquals(A, a)


class TestYaml2Yaml(unittest.TestCase):
    def testCleanup(self):
        cmd = [SCRIPT, '-i',
               os.path.join(TEST_DIR, 'qcm_python.yml'),
               '--dump-yaml', '--cleanup']
        out = subprocess.check_output(cmd, stderr=DEVNULL)
        self.assertEquals(out, """questions:
- answers:
  - feedback: Non, pas toujours.
    ko: contient toujours un nombre entier
  - feedback: En effet (mais une variable peut contenir d'autres valeurs)
    ok: peut contenir un nombre entier
  - ko: est visible depuis l'ensemble du programme
  title: En Python, une variable ...
  type: E
title: QCM sur le cours 1

""")

    def testDump(self):
        cmd = [SCRIPT, '-i',
               os.path.join(TEST_DIR, 'qcm_python.yml'),
               '--dump-yaml']
        out = subprocess.check_output(cmd, stderr=DEVNULL)
        self.assertEquals(out, """attempts: 0
description: ''
feedbackFinal: ''
questions:
- answers:
  - feedback: Non, pas toujours.
    ko: contient toujours un nombre entier
  - feedback: En effet (mais une variable peut contenir d'autres valeurs)
    ok: peut contenir un nombre entier
  - ko: est visible depuis l'ensemble du programme
  category: ''
  description: ''
  score: 1
  title: En Python, une variable ...
  type: E
title: QCM sur le cours 1

""")


if __name__ == "__main__":
    unittest.main()
