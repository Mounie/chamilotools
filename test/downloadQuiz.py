#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import mechanize
import zipfile
from StringIO import StringIO

# import logging
# logger = logging.getLogger("mechanize")
# logger.addHandler(logging.StreamHandler(sys.stdout))
# logger.setLevel(logging.DEBUG)

from createQuizChamilo import (
    connectOrResetChamilo,
    CLIOptions,
    AuthenticationError
)


def downloadQuiz(username, password, courseName, exerciseId,
                 br=None):
    br = connectOrResetChamilo(br, username, password, courseName)
    br.follow_link(text_regex="Exercices")
    try:
        br.follow_link(url_regex='choice=exportqti2&exerciseId=' + str(exerciseId) + '$')
    except mechanize._mechanize.LinkNotFoundError:
        print "No such exercice: " + str(exerciseId)
        return br, ""
    # We're downloading a ZIP file whose only content is an XML file.
    zip = StringIO(br.response().get_data())
    zfp = zipfile.ZipFile(zip, "r")
    xml_name = next(a for a in zfp.namelist() if a.endswith('.xml'))
    return br, zfp.read(xml_name)


def main(args):
    global br
    o = CLIOptions(args)
    o.read_password()
    br, xml = downloadQuiz(o.username, o.password, o.courseName, o.exerciseId)
    print xml

if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except AuthenticationError:
        sys.exit(1)
