#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


# Installation instructions:
#
# pip install --user mediawiki xlrd xlwt BeautifulSoup mechanize pyyaml
# ./createQuizChamilo.py ... quiz.xls

import re
import mechanize
import urllib
import mediawiki
import HTMLParser
import xlrd
import sys
import getopt
import getpass
import BeautifulSoup
import webbrowser
import cgi
import copy
import traceback
urlBase = "http://chamilo2.grenet.fr/inp"
urlChamilo = urlBase + "/main/auth/cas/logincas.php?firstpage="
urlBaseCourse = urlBase + "/courses"

# import logging
# logger = logging.getLogger("mechanize")
# logger.addHandler(logging.StreamHandler(sys.stdout))
# logger.setLevel(logging.DEBUG)


def encode(x):
    assert x is not None
    if isinstance(x, unicode):
        return x.encode('utf-8')
    else:
        return str(x)


def toUnicode(x):
    if not isinstance(x, unicode):
        return x.decode('utf-8')
    else:
        return unicode(x)


def getCellStr(sheet, r, c):
    try:
        cell = sheet.cell(r, c)
    except:
        return ""
    if cell.ctype == 1:
        return cell.value.encode('utf-8')
    else:
        return str(cell.value)


def openQuiz(path):
    if re.match('.*\.xlsx?', path):
        d, a = openQuizXls(path)
    elif re.match('.*\.yml', path):
        d, a = openQuizYaml(path)
    else:
        print "Please, provide a XLS(X) or a Yaml file describing the quizz."
        raise CLIError()
    return checkAndFillQuiz(d, a)


def openQuizXls(path):
    # Réouverture du classeur
    classeur = xlrd.open_workbook(path)
    # Récupération du nom de toutes les feuilles sous forme de liste
    nom_des_feuilles = classeur.sheet_names()
    # Récupération de la première feuille
    feuille = classeur.sheet_by_name(nom_des_feuilles[0])
    # Lecture des cellules:
    descriptionQuiz = dict()
    asks = []
    if feuille.cell_value(0, 0) == 'Quiz':
        descriptionQuiz = dict()
        descriptionQuiz["title"] = getCellStr(feuille, 0, 1)
        # descriptionQuiz["description"] = getCellStr(feuille, 0, 2)
        codeSyntax = getCellStr(feuille, 0, 2)
        descriptionQuiz["syntax"] = codeSyntax if codeSyntax else None
        descriptionQuiz["attempts"] = 2
        descriptionQuiz["feedbackFinal"] = "Retour global:"
        l = 1
        ask = dict()
        nbTrue = 0
        while l < feuille.nrows:
            if getCellStr(feuille, l, 0) == "GlobalFeedback":
                descriptionQuiz["feedbackFinal"] = getCellStr(feuille, l, 1)
            if getCellStr(feuille, l, 0) == "Question":
                if ask:
                    if not ask["type"]:
                        if ask["answers"]:
                            if nbTrue == 1:
                                ask["type"] = "C"
                            else:
                                ask["type"] = "G"
                        else:
                            ask["type"] = "O"
                    asks.append(ask)
                ask = dict()
                nbTrue = 0
                ask["answers"] = []
                try:
                    ask["category"] = getCellStr(feuille, l, 3)
                except IndexError:
                    ask["category"] = ""
                ask["type"] = getCellStr(feuille, l, 2)
                ask["title"] = getCellStr(feuille, l, 1)
                ask["description"] = ""
                ask["score"] = 5
                ask["feedback"] = "Bravo!"
                ask["feedbackFalse"] = "Recommencez!"
            elif feuille.cell_value(l, 0) == "EnrichQuestion":
                ask["description"] = getCellStr(feuille, l, 1)
                codeSyntax = getCellStr(feuille, l, 3)
                if codeSyntax:
                    ask["syntaxDesc"] = codeSyntax
            elif feuille.cell_value(l, 0) == "Score":
                ask["score"] = float(getCellStr(feuille, l, 2))
            elif feuille.cell_value(l, 0) == "FeedbackTrue":
                ask["feedback"] = getCellStr(feuille, l, 1)
            elif feuille.cell_value(l, 0) == "FeedbackFalse":
                ask["feedbackFalse"] = getCellStr(feuille, l, 1)
            elif feuille.cell_value(l, 0)[0:6] == "Answer":
                codeSyntax = getCellStr(feuille, l, 3)
                if codeSyntax:
                    ask["syntaxAnswer"] = codeSyntax
                if ask["type"] == "T":
                    ask["answers"] = getCellStr(feuille, l, 1)
                elif ask["type"] == "A":
                    if feuille.cell_value(l, 1):
                        ask["answers"].append(
                            {'text': getCellStr(feuille, l, 1),
                             'correct': getCellStr(feuille, l, 2)})
                    else:
                        ask["answers"].append(
                            {'text': None,
                             'correct': getCellStr(feuille, l, 2)})
                else:
                    if getCellStr(feuille, l, 2).strip():
                        nbTrue += 1
                    ask["answers"].append({'text': getCellStr(feuille, l, 1),
                                           'correct': getCellStr(feuille, l, 2).strip() != ""})
                    # TODO: implement feedback for XLS quiz
            l += 1
            if l == feuille.nrows:
                if not ask["type"]:
                    if ask["answers"]:
                        if nbTrue == 1:
                            ask["type"] = "C"
                        else:
                            ask["type"] = "G"
                    else:
                        ask["type"] = "O"
                asks.append(ask)
    return (descriptionQuiz, asks)


def vectorize(d):
    return [{k: v} for (k, v) in d.iteritems()]


def flatten(vect):
    d = dict()
    for unit_dict in vect:
        for k, v in unit_dict.iteritems():
            d[k] = v
    return d


def is_multiple_choice(t):
    return t in ('E', 'C', 'G', 'M', 'MVF', 'MVFE')


def yaml2quiz_answer(answer):
    if 'ok' in answer:
        a = {'text': answer['ok'],
             'correct': True}
    else:
        a = {'text': answer['ko'],
             'correct': False}
    if 'feedback' in answer:
        a['feedback'] = answer['feedback']
    return a


def quiz2yaml_answer(answer):
    res = dict(answer)
    if res["correct"]:
        res["ok"] = res["text"]
    else:
        res["ko"] = res["text"]
    del res["correct"]
    del res["text"]
    return res


def yaml2quiz_A_answer(answer):
    if isinstance(answer, dict):
        k, v = answer.keys(), answer.values()
        assert len(k) == 1  # TODO: friendly message
        return {"text": unicode(k[0]), "correct": unicode(v[0])}
    if isinstance(answer, basestring):
        return {"text": None, "correct": answer}
    error("Badly formed answer: %s" % answer)


def quiz2yaml_A_answer(answer):
    t, c = answer["text"], answer["correct"]
    if t is None:
        return c
    else:
        return {t: c}


def dump_yaml(x):
    from yaml import safe_dump
    return safe_dump(x, default_flow_style=False,
                     encoding='utf-8', allow_unicode=True)


def pretty_yaml(x):
    if isinstance(x, basestring):
        return x
    else:
        return dump_yaml(x)


def check_type(fieldname, value, expected):
    if not isinstance(value, expected):
        error("'%s' field must be a %s, got a %s instead:\n%s" % (
              fieldname, expected.__name__, type(value).__name__,
              pretty_yaml(value)))


def yaml2quiz_question(question):
    res = dict(question)
    if 'type' not in res:
        res['type'] = 'C'  # default value
    t = res['type']
    if 'answers' not in res:
        if 'answer' in res:
            error("misspelling of 'answers' as 'answer':\n%s" %
                  dump_yaml(res))
        else:
            error("question has no 'answers' field:\n%s" %
                  dump_yaml(res))
    answers = res['answers']
    if is_multiple_choice(t):
        check_type('answers', answers, list)
        res['answers'] = [yaml2quiz_answer(x) for x in res['answers']]
    if t == 'A':
        check_type('answers', answers, list)
        res['answers'] = [yaml2quiz_A_answer(x) for x in res['answers']]
    return res


def quiz2yaml_question(question):
    res = dict(question)
    t = res['type']
    if is_multiple_choice(t):
        res['answers'] = [quiz2yaml_answer(x) for x in res['answers']]
    if t == 'A':
        res['answers'] = [quiz2yaml_A_answer(x) for x in res['answers']]
    return res


def quiz2yaml(d, a):
    res = dict(d)
    res['questions'] = [quiz2yaml_question(x) for x in a]
    return res


def yaml2quiz(q):
    d = dict(q)
    a = [yaml2quiz_question(x) for x in d['questions']]
    del d['questions']
    return d, a


def openQuizYaml(path):
    from yaml import load, parser
    try:
        yamlQuiz = load(file(path, 'r'))
    except parser.ParserError as e:
        print e
        raise LoadError
    return yaml2quiz(yamlQuiz)


def convertWikiImg(desc, courseName):
    return re.sub(r"\[\[File:(.*?)\]\]", "<html><img src='" +
                  urlBaseCourse + "/" + courseName + "/document/\g<1>'></html>", desc)


def convertMath(desc, courseName):
    sout = ""
    while True:
        m = re.search(r"<math>(.*?)</math>", desc, flags=re.M | re.DOTALL)
        if not m:
            sout += convertWikiImg(desc, courseName)
            break
        eq = m.group(1).strip()
        s = convertWikiImg(desc[:m.start()], courseName)
        sout += s
        sout += '<html><img ' + \
            '" src="http://chamilo2.grenet.fr/cgi-bin/mimetex.cgi?' + \
                urllib.quote(eq) + '" /></html>'
        desc = desc[m.end():]
    return sout


def convert_desc(s, syntax, q_type, field, courseName):
    if s is None:
        return None

    # Preprocess source
    if syntax is None:
        return encode(s)
    elif syntax == "mediawiki":
        assert courseName
        s = convertMath(s, courseName)
        s = mediawiki.wiki2html(s, False)
        s = s.replace('\n\n', '<br />')
    elif syntax == "pre" or syntax == "escape":
        if not s:
            return ""
        s = toUnicode(s)  # To allow proper HTML-encoding later
        s = cgi.escape(s)  # Encode HTML characters
        s = s.replace('\n', '<br />')
        # Encode non-ascii into &...;
        s = s.encode('ascii', 'xmlcharrefreplace')
    else:
        raise ValueError("Unsupported syntax %s" % syntax)

    # Chamilo seems to automatically unescape what we give it, but
    # only in some cases.
    # Double-escape so that the end result is actually a
    # single-escaping.
    if q_type == "T" and field == "answer":
        s = cgi.escape(s)

    # Post-process (must come after escaping)
    if syntax == "pre":
        s = "<pre>%s</pre>" % s

    return encode(s)


def create_new_choice(br, ask, feedback, descriptionQuiz, courseName):
    syntax = descriptionQuiz.get("syntax")
    syntax = ask.get("syntax", syntax)
    syntax_answer = ask.get("syntaxAnswer", syntax)
    syntax_desc = ask.get("syntaxDesc", syntax)

    # Make a new choice
    # T : Text to be complete (blank part)
    # A : Elements to be associated
    # O : Open ask (no point by default, need the correction of teacher)
    # C : Only one solution to be selected
    # U : Only One one solution with the possibility to select "I don't know"
    # M : Multiple choices are possibles, each true answer has positive points, each wrong answer has negative points
    # E : Multiple choices are possibles, the points are affected only if the exact combination is selected
    # G : Multiple choices are possibles, the points affected is function depending of the part of exact combination
    # MVF : Multiple choices are possibles, similar M but with "I don't know" choice
    # MVFE : Multiple choices are possibles, similar E  but with "I don't
    # know" choice
    type = ask.get("type", "C")

    # Create new question
    if type == "U":
        br.follow_link(url_regex="answerType=10")
    elif type == "O":
        br.follow_link(url_regex="answerType=5")
    elif type == "M":
        br.follow_link(url_regex="answerType=2")
    elif type == "E":
        br.follow_link(url_regex="answerType=9")
    elif type == "G":
        br.follow_link(url_regex="answerType=14")
    elif type == "MVF":
        br.follow_link(url_regex="answerType=11")
    elif type == "MVFE":
        br.follow_link(url_regex="answerType=12")
    elif type == "A":
        br.follow_link(url_regex="answerType=4")
    elif type == "T":
        br.follow_link(url_regex="answerType=3")
    else:
        type = "C"
        br.follow_link(url_regex="answerType=1")
    # Set to the right number of questions
    nb_answers = len(ask["answers"])
    for f in br.forms():
        br.form = f
        break
    # Read the page to find value for categories
    soup = BeautifulSoup.BeautifulSoup(br.response().read())
    dicCat = {x.string: x["value"] for x in soup.find(
        'select', {"name": "questionCategory"}).findAll('option')}
    br.form["questionName"] = encode(ask["title"])
    br.form["questionDescription"] = encode(convert_desc(
        ask["description"], syntax_desc, type, "description", courseName))
    if ask["category"]:
        br.form["questionCategory"] = [dicCat[ask["category"]]]
    if type == "O":
        pass
    elif type == "T":
        br.form["answer"] = convert_desc(ask["answers"],
                                         syntax_answer, type,
                                         "answer", courseName)
        # Possible separators with Chamilo
        separators = {'[': "0", '{': "1", '(': "2", '*': "3", '#': "4", '%': "5", '$': "6"}
        sep_regex = {'[': r"\[(.*?)\]",
                     '{': r"\{(.*?)\}",
                     '(': r"\((.*?)\)",
                     '*': r"\*(.*?)\*",
                     '#': r"#(.*?)#",
                     '%': r"%(.*?)%",
                     '$': r"\$(.*?)\$"}
        sep = ask['separator']
        br.form["select_separator"] = [separators[sep]]
        i = 0
        for word in re.findall(sep_regex[sep], br.form["answer"]):
            br.form.new_control(
                "text", "sizeofinput[" + str(i) + "]", {"value": str(len(word) * 10)})
            br.form.new_control(
                "text", "weighting[" + str(i) + "]", {"value": str(ask["score"])})
            i += 1
    elif type == "A":
        for control in br.form.controls:
            if re.match("matches\[", control.name) or re.match("option\[", control.name):
                br.form.controls.remove(control)
        a = ask["answers"]
        left = [x["text"] for x in a if x["text"]]
        right = {x["correct"] for x in a}
        left = {x: i + 1 for x, i in zip(left, range(len(left)))}
        br.form.find_control("nb_matches").readonly = False
        br.form["nb_matches"] = str(len(left))
        br.form.find_control("nb_options").readonly = False
        br.form["nb_options"] = str(len(right))
        i = 1
        new_right = dict()
        for option in right:
            br.form.new_control(
                "text", "option[" + str(i) + "]", {"value": encode(option)})
            new_right[option] = i
            i += 1
        right = new_right
    else:
        br.form.find_control("nb_answers").readonly = False
        br.form["nb_answers"] = str(nb_answers)
    if type != "T" and type != "O":
        i = 1
        for q in ask["answers"]:
            text = q["text"]
            correct = q["correct"]
            if type != "A":
                br.form.new_control(
                    "hidden", "counter[" + str(i) + "]", {"value": str(i)})
                br.form.new_control(
                    "text", "comment[" + str(i) + "]", {"value": ask["feedback"] if feedback else ""})
            if "feedback" in q:
                br.form["comment[" + str(i) + "]"] = encode(q["feedback"])
            try:
                br.form["answer[" + str(i) + "]"] = convert_desc(text, syntax_answer,
                                                                 type,
                                                                 "answer", courseName)
            except:
                value = convert_desc(text, syntax_answer, type, "answer", courseName)
                br.form.new_control(
                    "text", "answer[" + str(i) + "]", {"value": value})
            if type != "E" and type != "G" and type != "MVF" and type != "MVFE":
                try:
                    br.form["weighting[" + str(i) + "]"] = str(ask["score"]) if correct else "-" + ask[
                        "score"] if type == "M" else "0"
                except:
                    br.form.new_control("text", "weighting[" + str(i) + "]", {"value": ask[
                                        "score"] if correct else "-" + str(ask["score"]) if type == "M" else "0"})
            if type == "A":
                br.form.new_control(
                    "text", "matches[" + str(i) + "]", {"value": encode(right[correct])})
            elif type == "MVF":
                br.form.new_control(
                    "checkbox", "correct[" + str(i) + "]", {"value": "1" if correct else "2", "checked": "checked"})
            elif correct:
                if type == "M" or type == "E" or type == "G" or type == "MVFE":
                    br.form.new_control(
                        "checkbox", "correct[" + str(i) + "]", {"value": "1", "checked": "checked"})
                else:
                    br.form.new_control("checkbox", "correct", {
                                        "value": str(i), "checked": "checked"})
            i += 1
    if type == "E":
        br.form["weighting[1]"] = str(ask["score"])
    if type == "G" or type == "MVFE":
        br.form["weighting[1]"] = str(ask["score"])
        # br.form.new_control("text","weighting[1]",{"value":"10"})
#        br.form.find_control("pts").items[0].selected=True
    if type == "U":
        # To add only if is an unique choice with I don't know
        br.form.new_control("hidden", "counter[666]", {"value": "666"})
        br.form.new_control("text", "answer[666]", {"value": "Ne sais pas"})
        br.form.new_control("text", "comment[666]", {
                            "value": encode(ask["feedback"]) if feedback else ""})
        br.form.new_control("text", "weighting[666]", {"value": "0"})
        # End of the part to add only for unique choice with I don't know
    if type == "MVF":
        br.form["option[1]"] = str(ask["score"])
        br.form["option[2]"] = "-" + str(ask["score"])
        br.form["option[3]"] = "0"
#    if type=="G":
#        print br.forms().next()
    br.submit("submitQuestion")
    # Return to asks list
    br.follow_link(text_regex="Retour")


def error(s):
    sys.stderr.write("ERROR: " + s + "\n")
    sys.exit(1)


def warn(s):
    sys.stderr.write("WARNING: " + s + "\n")


def setDefault(d, k, v):
    if k not in d:
        # Use a copy to avoid aliases, which would confuse the Yaml
        # dumper.
        d[k] = copy.copy(v)


QUIZ_DEFAULTS = {
    'attempts': 0,
    'feedbackFinal': '',
    'description': '',
}

QUESTION_DEFAULTS = {
    'score': 1,
    'category': '',
    'description': '',
}

QUESTION_TYPE_DEFAULTS = {
    'T': {'separator': '%'},
}


def checkAndFillQuiz(d, a):
    for q in a:
        i = 0
        if 'title' not in q:
            i += 1
            error("Please provide a title for question %d\n%s" % (i, q.__repr__()))
        if 'score' not in q:
            warn("No score set for question \"%s\"." % q['title'])
        q_default = QUESTION_TYPE_DEFAULTS.get(q['type'], dict())
        default = dict(QUESTION_DEFAULTS, **q_default)
        for k in default:
            setDefault(q, k, default[k])
    if 'title' not in d:
        error("Please provide a title for the quiz")
    for k in QUIZ_DEFAULTS:
        setDefault(d, k, QUIZ_DEFAULTS[k])
    return d, a


def cleanDefaultValues(d, a):
    for q in a:
        q_default = QUESTION_TYPE_DEFAULTS.get(q['type'], dict())
        default = dict(QUESTION_DEFAULTS, **q_default)
        for k in default:
            if q[k] == default[k]:
                del q[k]
    for k in QUIZ_DEFAULTS:
        if d[k] == QUIZ_DEFAULTS[k]:
            del d[k]
    return d, a


class LoadError(Exception):
    pass


class AuthenticationError(Exception):
    pass


class CLIError(Exception):
    pass


def connectAgalan(username, password):
    # Connect Agalan
    br = mechanize.Browser()
    br.open("https://cas-inp.grenet.fr/login")
    for f in br.forms():
        br.form = f
        br["username"] = username
        br["password"] = password
    response = br.submit()
    html = response.read()
    if "Les informations transmises n'ont pas permis de vous authentifier" in html:
        h = HTMLParser.HTMLParser()
        try:
            import html2text
            print html2text.html2text(h.unescape(html.decode('utf-8')))
        except:
            print "Invalid login or password"
        raise AuthenticationError()
    return br


def connectOrResetChamilo(br, username, password, courseName):
    if br is None:
        return connectChamilo(username, password, courseName)
    else:
        resetChamilo(br, courseName)
        return br


def resetChamilo(br, courseName):
    br.open(urlChamilo + courseName)
    if not br.geturl().endswith(courseName + '/'):
        # for some reason, the first attempt usually redirects to
        # user_portal.php (through Location: header).
        br.open(urlChamilo + courseName)


def deleteOrphans(br, courseName):
    br.open(urlBase + "/main/coursecopy/recycle_course.php?cidReq=" +
            courseName + "&id_session=0&gidReq=0",
            "recycle_option=select_items")
    for f in br.forms():
        break
    br.form = f
    br.form["resource[quiz][-1]"] = ["on"]
    br.submit()


def connectChamilo(username, password, courseName):
    br = connectAgalan(username, password)
    resetChamilo(br, courseName)
    return br


def buildFeedback(feedbackGeneral, asks, syntax, courseName=""):
    feedback = "<h2 id='w_retour-global'> " + feedbackGeneral + "</h2><ul>"
    i = 1
    for ask in asks:
        if ask["type"] != "O":
            feedback += ("<li> " + "<div class='ok'>Question " + str(i) + " : " + convert_desc(ask.get(
                "feedback", ""), syntax, ask["type"], "feedback", courseName) +
                "</div>" + "<div class='ko'>Question " +
                str(i) + " : " +
                convert_desc(ask.get("feedbackFalse", ""), syntax, ask["type"], "feedback", courseName) +
                "</div></li>"
            )
        else:
            feedback += "<li> " + "<div>Question " + \
                str(i) + " : " + convert_desc(ask.get("feedback", ""),
                                              syntax, ask["type"],
                                              "feedback", courseName) + "</div></li>"
        i += 1
    feedback += r"""</ul><script>$( function(){
  $("span[style='color: #FF0000; text-decoration: line-through;']").parent().find("span[style='color: #008000;']").hide();
  okAnswer=$("h3:contains('Exact')").parents(".question_row").find(".page-header h3")
  okAnswer.each(function() {
    indice=$(this).text()
    indice=indice.substr(0,indice.indexOf('.'))
    $('#w_retour-global').parent().find("li:nth-child("+indice+") .ko").hide()
  })
  koAnswer=$("h3:contains('Faux')").parents(".question_row").find(".page-header h3")
  koAnswer.each(function() {
    indice=$(this).text()
    indice=indice.substr(0,indice.indexOf('.'))
    $('#w_retour-global').parent().find("li:nth-child("+indice+") .ok").hide()
  })
})</script>
"""
    return feedback


def createCategory(br, asks):
    br.follow_link(text_regex="Exercices")
    br.follow_link(url_regex="tests_category.php")
    soup = BeautifulSoup.BeautifulSoup(br.response().read())
    categories = {c["category"] for c in asks if c["category"]}
    for c in categories:
        if not soup.findAll('div', {"class": "sectiontitle"}, text=c):
            br.follow_link(text_regex="Ajouter")
            for f in br.forms():
                br.form = f
            br["category_name"] = c
            br.submit()


def createQuiz(descriptionQuiz, asks, o, br=None):
    syntax = descriptionQuiz.get("syntax")
    br = connectOrResetChamilo(br, o.username, o.password, o.courseName)
    createCategory(br, asks)
    resetChamilo(br, o.courseName)
    # Create new Exercise
    br.follow_link(text_regex="Exercices")
    # Delete if exist
    if o.addMode in ("1", "3"):
        idsQuiz = []
        for l in br.links(url_regex="overview.php"):
            # The link text may be truncated (end replaced
            # with "..."). The title="..." attribute seems
            # reliable when present.
            titleFound = False
            quizFound = False
            for k, v in l.attrs:
                if k == 'title':
                    titleFound = True
                    if toUnicode(v) == toUnicode(descriptionQuiz["title"]):
                        quizFound = True
            if not titleFound:
                # No "title=" attribute. The title was not truncated,
                # we can look at the link text.
                if toUnicode(l.text) == toUnicode("[IMG] " + descriptionQuiz["title"]):
                    quizFound = True
            if quizFound:
                idsQuiz.append(re.findall("exerciseId=([0-9]+)", l.url)[0])
        for id in idsQuiz:
            br.follow_link(url_regex="choice=delete.*exerciseId=" + id)
            # Following the link brings us back to a page with the
            # list => no need to "br.back()".
    elif o.addMode == "2":
        try:
            l = br.find_link(
                text_regex="\[IMG\] " + descriptionQuiz["title"] + "$")
            return "Exercise exist", -1, br
        except:
            pass
    br.follow_link(text_regex="Nouveau")
    if descriptionQuiz["feedbackFinal"] == "Retour global:":
        feedback = buildFeedback(
            descriptionQuiz["feedbackFinal"], asks, syntax, o.courseName)
    else:
        feedback = descriptionQuiz["feedbackFinal"]
    for f in br.forms():
        br.form = f
        br["exerciseTitle"] = encode(descriptionQuiz["title"])
        br["exerciseDescription"] = encode(descriptionQuiz["description"])
        br["exerciseFeedbackType"] = ["0" if o.feedback else "2"]
        br["results_disabled"] = [o.mode]
        br["display_category_name"] = ["0"]
        br["randomAnswers"] = ["1" if o.randomAnswers else "0"]
        br["randomByCat"] = ["1" if o.randomCategory else "0"]
        br["randomQuestions"] = ["1" if o.randomCategory else "0"]
        if o.timeLimit != "0":
            br.find_control(name="enabletimercontrol").items[0].selected = True
            br["enabletimercontroltotalminutes"] = o.timeLimit
        br["pass_percentage"] = o.passPercent
        br["exerciseType"] = ["1" if o.onePage else "2"]
        br["exerciseAttempts"] = [str(descriptionQuiz["attempts"])]
        br["text_when_finished"] = feedback
    br.submit()

    # Get the "preview" full URL
    l = br.find_link(url_regex="overview.php")
    urlQuiz = l.absolute_url

    # Which exercice are we in?
    match = re.match('.*exerciseId=([0-9]*).*', br.geturl())
    if match:
        idQuiz = match.group(1)
    else:
        raise AssertionError("URL " + br.geturl() +
                             " does not contain exerciseId=...")

    if o.hidden:
        br.follow_link(
            url_regex="/exercice.php\?.*cidReq=" + o.courseName)
        br.follow_link(
            url_regex="(^|/)exercice.php\?.*&choice=disable&.*&exerciseId=" + idQuiz)
        br.follow_link(
            url_regex="(^|exercice/)admin.php\?.*&exerciseId=" + idQuiz)

    for ask in asks:
        try:
            create_new_choice(br, ask, False, descriptionQuiz, o.courseName)
        except:
            print("Error question in Chamilo:")
            print(pretty_yaml(ask))
            traceback.print_exc(file=sys.stdout)
            raise LoadError()
    # Delete orphans for addMode=="1"
    if o.addMode == "3":
        deleteOrphans(br, o.courseName)
        resetChamilo(br, o.courseName)
        br.follow_link(text_regex="Exercices")
    return urlQuiz, idQuiz, br


def createQuizFromXls(filename, options):
    (d, a) = openQuizXls(filename)
    return createQuiz(d, a, options)


def deleteQuiz(username, password, courseName, exerciseId,
               br=None):
    br = connectOrResetChamilo(br, username, password, courseName)
    br.follow_link(text_regex="Exercices")
    try:
        br.follow_link(url_regex='choice=delete.*&exerciseId=' + str(exerciseId) + '$')
    except mechanize._mechanize.LinkNotFoundError:
        print "No such exercice: " + str(exerciseId)
        return br, ""
    return br


def usage():
    print """\
Syntax: createQuizChampilo.py [-h] -u <user> -c <course>
        [-w] [-t 2] [-f] [-a | -e | -s] [-o] [-r] [-m 1]
        <quiz_files>...

    Upload <quiz_files> to Chamilo course <course> authenticating with user <user>.

    -u username : Grenoble INP's Chamilo user login
    -c coursename : Grenoble INP's Chamilo course name
    -w : if elements are described with wikimedia syntax in Excel file
            can be used  <math>LatexCode </math> see http://www.forkosh.com/mimetex.html
            can be used [[File:PathToImage]] to insert a picture, where
            PathToImage is a relative Path from the document directory's
            courses. url generated is:
            http://URLCHAMILO/courses/COURSENAME/document/PathToImage
            For Yaml Quiz, an alternative is to use 'syntax: mediawiki' in
            the header or in individual questions.
    -t nb : number of try to the student (infinity by default)
    -p percent : percentage needed to succeed the test (0 by default)
    -l time_limit: time limit in minutes to pass the test (no limit by default)
    -f : feedback disable
    -a : auto learning mode
    -e : exercise mode
    -s : exam mode
    -o : all the questions on one page (one page per question by default)
    -r : random by category
    -m : mode to add
         0 add (default)
         1 remove the quiz with the same name and add the new one,
         2 add only if no quiz exists with the same name
         3 remove orphans asks and remove the quiz with the same name
         and add the new one, CAUTION : DON'T USE THE COURSE USE CATEGORIES
    --random-answers : random answers
    --dump-yaml : dump yaml
    --view : launch browser on generated online quiz
    --hidden : create the quiz, but hide it from students

Deprecated options:

    -i <quiz_file> : file containing the quiz (equivalent to using
                     <quiz_files> as positional argument).
    """


class CLIOptions(object):

    def __init__(self, argv):
        self.username = ''
        self.courseName = ''
        self.filenames = []
        self.wiki = False
        self.feedback = True
        self.nb = 0
        self.mode = "2"
        self.passPercent = "0"
        self.timeLimit = "0"
        self.onePage = False
        self.randomCategory = False
        self.addMode = "0"
        self.dumpYaml = False
        self.debugMode = False
        self.cleanUp = False
        self.exerciseId = 0
        self.randomAnswers = False
        self.view = False
        self.showHelp = False
        self.hidden = False
        try:
            opts, args = getopt.gnu_getopt(
                argv, "dhwu:p:i:c:t:p:l:faesorm:",
                ["dump-yaml", "cleanup", "exercise-id=",
                 "random-answers", "view", "hidden"])
        except getopt.GetoptError:
            usage()
            raise CLIError()
        for opt, arg in opts:
            if opt == '-h':
                self.showHelp = True
            elif opt == "-d":
                self.debugMode = True
            elif opt == '-u':
                self.username = arg
            elif opt == '-c':
                self.courseName = arg
            elif opt == '-i':
                self.filenames.append(arg)
            elif opt == '-w':
                self.wiki = True
            elif opt == '-t':
                self.nb = int(arg)
            elif opt == '-p':
                self.passPercent = str(arg)
            elif opt == '-l':
                self.timeLimit = str(arg)
            elif opt == '-f':
                self.feedback = False
            elif opt == '-a':
                self.mode = "0"
            elif opt == '-e':
                self.mode = "2"
            elif opt == '-s':
                self.mode = "1"
            elif opt == '-o':
                self.onePage = True
            elif opt == '-r':
                self.randomCategory = True
            elif opt == '--random-answers':
                self.randomAnswers = True
            elif opt == '-m':
                self.addMode = str(arg)
            elif opt == '--dump-yaml':
                self.dumpYaml = True
            elif opt == '--cleanup':
                self.cleanUp = True
            elif opt == '--exercise-id':
                self.exerciseId = int(arg)
            elif opt == '--view':
                self.view = True
            elif opt == '--hidden':
                self.hidden = True
            else:
                print "Invalid option: %s" % opt
                usage()
                raise CLIError()
        self.filenames += args

    def read_password(self):
        self.password = getpass.getpass()


def processQuiz(o, filename):
    d, a = openQuiz(filename)
    if o.debugMode:
        print a
        sys.exit()
    d["attempts"] = o.nb
    if o.wiki:
        d["syntax"] = "pre"
    if o.dumpYaml:
        if o.cleanUp:
            d, a = cleanDefaultValues(d, a)
        y = quiz2yaml(d, a)
        print dump_yaml(y)
    else:
        o.read_password()
        # NEEDSWORK: we could reuse the same connection to Chamilo to
        # save a few HTTP round-trips.
        url, idQuiz, br = createQuiz(d, a, o)
        if o.view:
            webbrowser.open(url)
        else:
            print url


def main(argv):
    o = CLIOptions(argv)
    if o.showHelp:
        usage()
        return
    if (o.username and o.courseName and o.filenames) or o.dumpYaml:
        for filename in o.filenames:
            processQuiz(o, filename)
    else:
        usage()
        raise CLIError()


if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except AuthenticationError:
        sys.exit(1)
    except CLIError:
        sys.exit(1)
    except LoadError:
        sys.exit(1)
