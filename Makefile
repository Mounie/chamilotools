PY = ${shell git ls-files '*.py'}

USER =
COURSE =
-include config.mk

.PHONY: static test check interactive quick
quick: static test
.NOTPARALLEL: check
check: quick interactive

static:
	for f in $(PY); do \
		python -m py_compile $$f || { echo; echo $$f; status=1; } \
	done; exit $$status
	pep8 $(PY) --max-line-length=125 --ignore=E402
	pyflakes $(PY)

test:
	./test/testCreateQuizChamilo.py

interactive:
	./test/testCreateDownloadQuiz.py -u $(USER) -c $(COURSE)
